﻿window.onload = function () {

    console.log(blackJack(19, 21));
    console.log(blackJack(21, 19));
    console.log(blackJack(19, 22));
    console.log(blackJack(22, 22));

};

function blackJack(number1, number2)
{
    
    if (number1 > 21 && number2 > 21)
    {
        return 0;
    }
    else if (number1 > 21 & number2 <= 21)
    {
        return number2;
    }
    else if (number2 > 21 & number1 <= 21)
    {
        return number1;
    }
    else
    {
        return Math.max(number1, number2);
    }

}