﻿window.onload = function () {

    var result = sumDouble(1, 2);
    console.log(result);
    
    var result2 = sumDouble(5, 5);
    console.log(result2);

    var result3 = sumDouble(2, 3);
    console.log(result3);
};

function sumDouble(number1, number2)
{
    if(number1 === number2)
    {
        return (number1 + number2) * 2
    }
    else {
        return number1 + number2;
    }
}