﻿window.onload = function () {
    /*
        This is a multi-line comment
        The window object is a DOM document
        Every tab in a browser that loads gets a window object
        The load event is triggered after the document loads and this code will run
    */
    // Single line comment - declaring variables below

    var numberOfClevelandTeams = 3;
    console.log("Number of Cleveland teams is " + numberOfClevelandTeams);

    var nameOfCity = "Cleveland";
    console.log("The name of the city is " + nameOfCity);

    var isMonday = true;
    console.log("Is Monday " + isMonday);

    var nameOfTeams = ["Cleveland Indians", "Cleveland Cavaliers", "Cleveland Browns"];
    console.log("The teams are named " + nameOfTeams);

    //Undefined
    var nameOfNewBrownsQuarterback;
    console.log("The name of the new Brown's quarterback is " + nameOfNewBrownsQuarterback);

    //New Assignment
    numberOfClevelandTeams = "three";
    console.log("Number of Cleveland teams is " + numberOfClevelandTeams);

    //Assignments

    var numberOfQuarterbacksSince99 = "24";
    console.log("Cleveland has had " + numberOfQuarterbacksSince99 + " quarterbacks since they returned in 1999");

    var hasRG3 = true;

    if (hasRG3) {
        numberOfQuarterbacksSince99++;
        console.log("With RG3 we've got " + numberOfQuarterbacksSince99 + " now");
    }

    //Equality Comparison

    var firstNumber = 1;
    var secondNumber = 1;
    var boolValue = true;
    var stringValue = "1";
    var arrayValue = [1];
    var undefinedValue;

    console.log("firstNumber is " + firstNumber);
    console.log("secondNumber is ") + secondNumber;
    console.log("boolValue is " + boolValue);
    console.log("stringValue is " + stringValue);
    console.log("arrayValue is " + arrayValue);
    console.log("undefinedValue is " + undefinedValue);

    console.log("firstNumber == secondNumber " + (firstNumber === secondNumber));
    console.log("firstNumber == boolValue " + (firstNumber === boolValue));
    console.log("firstNumber == stringValue " + (firstNumber === stringValue));
    console.log("boolValue == stringValue " + (stringValue === boolValue));
    console.log("firstNumber == arrayValue " + (firstNumber === arrayValue));
    console.log("firstNumber == undefinedValue " + (firstNumber === undefinedValue));
    console.log("null == undefinedValue " + (null === undefinedValue));

    //Using the TypeOf operator

    console.log("firstNumber is " + typeof firstNumber);
    console.log("arrayValue is " + typeof arrayValue);
    console.log("stringValue is " + typeof stringValue);
    console.log("undefinedValue is " + typeof undefinedValue);

    //Calling functions

    var addResult = Add(5, 3);
    console.log(addResult);

    var addResult2 = Add("Hello", "Mary");
    console.log(addResult2);

    //Nesting functions

    var squareResult = Square(6);
    console.log("Result of square result " + squareResult);

    function Square(number) {
        return number * number;
    }

    var square2Result = Square(10);
    console.log(square2Result);

    //Variable Scope
    //Variables declared within a function, even if it is nested, are only available within that function
    //function NestedFunctionVariable() {
    //    var subtract = "Add";
    //}
    //console.log(subtract);

    //if (true)   We can declare variables within an if block and they'll be available outside the block
    //{
    //    var hi = "HI";
    //}
    //console.log(hi);

    //Nested functions have the ability to access variables declared outside of them

    function InnerFunction()
    {
        console.log(numberOfQuarterbacksSince99);
    }

    InnerFunction();
    InnerFunction();

    //Calling a function without required parameters
    function WelcomeUser(username, welcomeMessage)
    {
        console.log(username + " " + welcomeMessage + "!");
    }
    
    WelcomeUser("Josh", "Good Afternoon");
    WelcomeUser("Eric");
    WelcomeUser("Lyndsey", null);

    //String Methods
    var word = "Tech Elevator";
    console.log(word);
    console.log("The length using .length property is " + word.length);
    console.log("The index of 'lev' is " + word.indexOf('lev'));
    console.log("The index of 'levitate' is " + word.indexOf('levitate'));
    console.log(word[0]);

    //Arrays
    var groceries = ["fruit", "beer", "noodles", "pasta sauce"]
    console.log(groceries.length);
    console.log(groceries);

    var roomatesGroceries = ["gummy bears", "couches"];
    var ultimateList = groceries.concat(roomatesGroceries);
    
    console.log(ultimateList);

    //Adding to array
    ultimateList.push("Milk");
    console.log(ultimateList.length);

    ultimateList[1] = "MGD";
    console.log(ultimateList);

    ultimateList[15] = "Tide";
    console.log(ultimateList);
    console.log(ultimateList.length);

    ultimateList.length = 1;
    console.log(ultimateList);
    console.log(ultimateList.length);

    //Math library
    console.log(Math.PI);
    console.log(Math.LOG10E);
    console.log(Math.random);



};

function Add (num1, num2)
{
    
    return num1 + num2;
}
