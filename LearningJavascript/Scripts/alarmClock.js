﻿window.onload = function () {

    console.log(alarmClock(1, false));
    console.log(alarmClock(5, true));
    console.log(alarmClock(0, false));
    console.log(alarmClock(6, true));

};

function alarmClock(dayOfWeek, onVacation)
{
    var weekArray = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"]

    if(dayOfWeek === 0 || dayOfWeek === 6)
    {
        if(onVacation)
        {
            return weekArray[dayOfWeek] + " off";
        }
        else
        {
            return weekArray[dayOfWeek] + " 10:00";
        }
    }
    else
    {
        if(onVacation)
        {
            return weekArray[dayOfWeek] + " 10:00"
        }
        else
        {
            return weekArray[dayOfWeek] + " 7:00"
        }
    }
}