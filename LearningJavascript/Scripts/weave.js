﻿window.onload = function () 
{
    var array1 = [1, 3, 5];
    var array2 = [2, 4];
    var array3 = [1, 3, 5];
    var array4 = [2, 4, 6, 8];
    console.log(weave(array1, array2));
    console.log(weave(array3, array4));

};

function weave (array1, array2)
{
    var maxArray = Math.max(array1.length, array2.length)
    var newArray = [];

    for (var i = 0; i < maxArray; i++)
    {
        if (i < array1.length)
        {
            newArray.push(array1[i]);
        }
        if (i < array2.length)
        {
            newArray.push(array2[i])
        }
        
    }
    return newArray;
}