﻿window.onload = function () {

    console.log(makeMiddle([1, 2, 3, 4]));
    console.log(makeMiddle([7, 1, 2, 3, 4, 9]));
    console.log(makeMiddle([1, 2]));
    console.log(makeMiddle([1, 2, 3]));
};

function makeMiddle(array)
{
    if (array.length % 2 !== 0 || array.length < 2)
    {
        return [];
    }
    else 
    {
        return array[(array.length / 2) - 1] + "," + array[array.length / 2];
    }
};